module.exports = function(grunt) {

    var jsFiles = [
        'jslabel.js'
    ];

    var tsFiles = [
        'label.ts',

        'main.ts'
    ];
    
    jsFiles = jsFiles.map(function(f) {
        return 'src/' + f;
    });

    var tsBuildFiles = tsFiles.map(function(f) {
        return 'build/' + f.replace('.ts', '.js');
    });

    tsFiles = tsFiles.map(function(f) {
        return 'src/' + f;
    });

    grunt.initConfig({
        ts: {
            default: {
                src: tsFiles.concat(['lib/*.d.ts']),
                outDir: 'build/',
                reference: 'src/stdafx.ts'
            },
            options: {
                compiler: 'node_modules/typescript/bin/tsc.js',
                comments: false
            }
        },
        concat: {
            default: {
                src: jsFiles.concat(tsBuildFiles),
                dest: 'test.js'
            },
            options: {
                sourceMap: true
            }
        },
        uglify: {
            default: {
                files: {
                    'test.min.js': ['test.js']
                },
                options: {
                    sourceMapIn: 'test.js.map',
                    sourceMap: true
                }
            }
        },
        watch: {
            files: tsFiles.concat(jsFiles).concat('Gruntfile.js'),
            tasks: ['default']
        }
    });

    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['ts', 'concat']);
    grunt.registerTask('release', ['default', 'uglify']);
};

