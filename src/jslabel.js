qx.Class.define('qxTest.JsLabel', {
    extend: qx.ui.basic.Label,

    construct: function(value) {
        this.base(arguments, value);
        this.setRich(true);
    },

    members: {
        setBold: function() {
            var val = this.getValue();
            this.setValue('<b>' + val + '</b>');
        }
    }
});
