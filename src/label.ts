/// <reference path="stdafx.ts" />

module qxTest {

    export class TestLabel extends qx.ui.basic.Label {

        constructor(value: string) {
            super(value);
            this.setRich(true);
        }

        setBold() {
            this.setValue('<b>' + this.getValue() + '</b>');
        }

    }

}
