/// <reference path="stdafx.ts" />

declare module qxTest {
    var JsLabel;
}

module qxTest {

    import form = qx.ui.form;

    function main(app: qx.application.Standalone) {
        var root = <qx.ui.container.Composite>app.getRoot();

        var btn = new form.Button('Push me!', 'resources/test.png');
        var lbl = new TestLabel('Hello, world!');
        var lbl2 = new qxTest.JsLabel('Hello too!');

        root.add(btn, {left: 50, top: 50});
        root.add(lbl, {left: 50, top: 150});
        root.add(lbl2, {left: 50, top: 250});

        btn.addListener('execute', () => {
            lbl.setBold();
            lbl2.setBold();
        });
    }

    qx.registry.registerMainMethod(main);

}
